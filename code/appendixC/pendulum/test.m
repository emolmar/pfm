% We test if drawBase.m is working with some random data
y =5;
width = 1;
height = 0.2;
gap = 0.5;
mode = 'normal';
handle = []
handle_Base = drawBase(y, width, height, gap, handle, mode);
% now we test drawRod
theta = pi/6;
L = 2;
hold on;
handle_Rod=drawRod(y, theta, L, gap, height, handle, mode);
if L*sin(theta) > width/2,
	xmax = L*sin(theta);
else
	xmax = width/2;
end

axis([y-xmax-gap y+xmax+gap 0 L*cos(theta)+height+2*gap]);
